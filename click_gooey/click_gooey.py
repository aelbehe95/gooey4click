"""
A click plugin that allows using gooey with a flag
"""
import click
from gooey import Gooey


def _build_gooey_ui_elements():
    pass


def _eager_callback(ctx, param, value):
    pass


def gui_option(show_gui_flag='--gui', gooey_settings=None):
    """
    Adds an eager cli option flag such that if specified, Gooey UI
    will be shown based on the click decorated options
    :param show_gui_flag:
    :param gooey_settings:
    :return:
    """
    def decorator(f):
        if not gooey_settings:
            Gooey()(f)
        else:
            gooey_settings(f)
        click.option(show_gui_flag,
                     is_flag=True, is_eager=True,
                     callback=_eager_callback)(f)
        return f
    return decorator
